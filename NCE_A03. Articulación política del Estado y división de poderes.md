###### Art. A03 - Articulación política del Estado y división de poderes ######
1. Los poderes del Estado se organizarán en órganos especiales de representación, de gobierno y judiciales con personalidad jurídica propia:
    1. A las _Asambleas de Representantes_ les corresponde la potestad de aprobar las normas centrales del ordenamiento jurídico, fijar el régimen de ingresos y gastos del Estado dentro de su ámbito de competencia, ejercer el control político sobre los Gobiernos de su ámbito territorial y las demás competencias que les atribuya la Constitución o no hayan sido atribuidas a otro órgano del Estado.
    2. Los órganos de _Gobierno_ dirigen la política del Estado y la Administración dentro de su ámbito de actuación, para lo cual ejercen la función ejecutiva, la potestad reglamentaria y cualesquiera otras competencias que tengan singularmente atribuidas, dentro de los límites que marque la ley.
    3. Los _órganos judiciales_ velan por el imperio de la ley, la seguridad jurídica y los derechos de las personas mediante el ejercicio de la potestad jurisdiccional; pacificando con sus decisiones los conflictos que se le planteen de forma definitiva e irrevocable.

2. La ley garantizará la separación de estos poderes en origen, su independencia respecto del resto de los poderes del Estado, el correcto funcionamiento de los mecanismos de control recíproco entre ellos, y el sometimiento de todos ellos al escrutinio público y al mandato de los ciudadanos.

> **Concordancias:** 
>
> **Equivalencia CE78:** 
>
* **rmatelec @??:??:??** Separación de poderes en origen, cada uno de ellos tiene un origne igualmente legítimo pero distinto, sin que medie delegación o nombramientos entre ellos. sus dictámenes o decisiones no pueden ser invalidados, modificados o anulados por otro órgano.

[Anterior](NCE_A02.%20Organizaci%C3%B3n%20pol%C3%ADtica%20de%20la%20Naci%C3%B3nEstado.md) | [Siguiente](NCE_A04.%20Territorio%20nacional.md)