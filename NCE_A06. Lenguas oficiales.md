###### Art. A06 - Lenguas oficiales

1. La lengua oficial del Estado es el español. Todos los españoles tienen el deber de conocerla y el derecho a usarla.
2. Las demás lenguas autóctonas del territorio nacional constituyen un patrimonio cultural que será objeto de especial respeto y protección. Las _Asambleas de Representantes_ de las entidades territoriales administrativas iguales o mayores que la Provincia podrán establecer dichas lenguas como cooficiales, en cuyo caso su uso tiene plena validez jurídica en las relaciones con los poderes públicos radicados en dicho territorio.
3. Las actuaciones de los _órganos judiciales_ se desarrollarán en español, salvo que el ámbito de actuación del órgano que conoce del asunto no exceda el territorio en el que una lengua sea cooficial y las partes personadas en el procedimiento acuerden expresamente usar dicha lengua.

> **Concordancias:** 
>
> **Equivalencia CE78:** [Art. 3](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a3): Lenguas Oficiales.
>

[Anterior](NCE_A05.%20S%C3%ADmbolos%20nacionales.md) | [Siguiente](NCE_B01.%20Supremac%C3%ADa%20de%20la%20Constituci%C3%B3n.md)