###### Art. B03 - Criterios de interpretación de las normas ######
1. El _precepto_ es el núcleo irreductible de interpretación de las normas, que corresponde con el artículo en las normas articuladas, y con el párrafo en aquellas no articuladas.
2. Cada _precepto_ sólo puede tener una interpretación, por lo que cuando no pudiera determinarse un único sentido o significado siguiendo los criterios contenidos en el presente artículo, o cuando el mismo resultase ambiguo, indeterminado o entrara en contradicción con otros _preceptos_ de la misma norma, no será aplicable.
3. Los _preceptos_ se interpretarán según los criterios y definiciones dados en la misma norma, si los hubiera. En su defecto, se interpretarán por el tenor literal de las palabras según el uso que de las mismas se hiciera en el momento en que se promulgó la norma; salvo cuando se trate de términos científicos, artísticos o técnicos, en los que prevalecerá el significado que tuvieran en el área de conocimiento a la que pertenezcan.
4. Si el criterio anterior no fuera suficiente para determinar el significado de un _precepto_, se interpretará de forma que sea coherente con el resto de _preceptos_ de la norma que los contiene.
5. Si los criterios anteriores no fueran suficientes para determinar un único significado, se acudirá a la norma en su conjunto para determinar el sentido o significado del _precepto_ en cuestión dentro de la misma, atendiendo a la finalidad perseguida por ésta. En caso de que prevalezca la duda se buscará la interpretación que sea coherente con el sistema legal en que se inserta la norma, atendiendo a la realidad social del tiempo en que ha de ser aplicada.
6. Las _preceptos_ relativos a los _derechos fundamentales_ y a las libertades que la Constitución reconoce se interpretarán de conformidad con la Declaración Universal de Derechos Humanos de 1948, y aquellos otros tratados internacionales sobre las mismas materias ratificados por España.
7. Los criterios de interpretación enunciados en este artículo son los únicos aplicables, quedando excluido cualquier otro y -en particular- los antecedentes históricos o legislativos, los trabajos de elaboración de _precepto_, la jurisprudencia de los órganos con _potestad jurisdiccional_, la doctrina -tanto científica como judicial- y las posibles consecuencias de la correcta interpretación de la norma.

> **Concordancias:** Disposición Transitoria XX (no aplicación retroactiva), F01 (obligación de vigilar la calidad técnica de la leyes)
>
>
> **Equivalencias:** [Art. 3.1 C.Civil](https://boe.es/buscar/act.php?id=BOE-A-1889-4763#art3),
>
* **rmatelec @2017.11.22** Los principios generales del derecho se aplican cuando no hay norma, no cuando no está claro su significado.

[Anterior](NCE_B02.%20Fuentes%20del%20ordenamiento%20jur%C3%ADrico.md) | [Siguiente](NCE_B04.%20Criterios%20de%20integraci%C3%B3n%20de%20las%20normas.md)
