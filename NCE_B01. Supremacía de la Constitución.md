### TITULO SEGUNDO. - De las normas y su interpretación

###### Art. B01 - Supremacía de la Constitución ######
1. Todas las personas y los poderes públicos están obligados al cumplimiento de esta Constitución, y sometidos tanto a sus preceptos como al ordenamiento jurídico de la que es fundamento.
2. Lo establecido en la Constitución es directa e inmediata aplicable, sin necesidad de legislación de desarrollo para ser fuente de derechos y obligaciones.
3. Las normas, disposiciones o actos que vulneren, contradigan o sean incompatibles con lo establecido en la Constitución son nulos de pleno derecho y carecen, por tanto, de fuerza vinculante sin necesidad de que se declare la inconstitucionalidad de los mismos. Tal circunstancia puede ser apreciada y alegada por cualquier persona.
4. Corresponde exclusivamente a la Constitución el definir 1) cuáles son las normas que integran el ordenamiento jurídico, 2) los procedimientos para su creación, modificación y derogación; 3) los criterios para su interpretación, 4) su orden de prelación y 5) los mecanismos para resolver los conflictos entre normas contradictorias.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 9.1](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a9),

[Anterior](NCE_A06.%20Lenguas%20oficiales.md) | [Siguiente](NCE_B02.%20Fuentes%20del%20ordenamiento%20jur%C3%ADrico.md)
