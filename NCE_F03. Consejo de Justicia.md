###### Art. F02 - Consejo de Justicia ######

1. El Consejo de Justicia es el órgano de gobierno del Poder Judicial, encargado de garantizar la independencia de la Administración de Justicia y de proveerla de los medios precisos para que desarrolle su función con eficacia.

2. Son competencia de este órgano:
    1. Elaborar, tramitar, dirigir la ejecución y controlar el cumplimiento del presupuesto de la Administración de Justicia.
    2. El ejercicio de la potestad reglamentaria en cuestiones concernientes a las personas y órganos adscritos a la Administración de Justicia, especialmente en lo relativo a:
        1. Nombramientos, ascensos y provisión de plazas.
        2. Actuaciones y procedimientos disciplinarios.
        3. Creación, modificación y supresión de plazas y órganos.
        4. La dirección, coordinación y organización de los recursos materiales y humanos.
    3. La inspección y vigilancia del correcto funcionamiento de la Administración de Justicia.
    4. La ejecución de los programas de asistencia jurídica del Estado.
    5. La cooperación jurídica internacional.
    6. Aquellas otras que se le atribuyan en la Constitución o mediante disposición con rango de Ley.

3. El Consejo de Justicia estará integrado por.... Organo de gobierno elegido por cuerpo electoral propio de caracter técnico

4. El miembro de mayor edad del Consejo de Justicia desempeñará la función de Notaria Mayor del Estado.

> **Concordancias:**
>
> **Equivalencia CE78: [Art.122](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a122)
>

https://es.wikipedia.org/wiki/Ministro_de_Justicia_de_Espa%C3%B1a