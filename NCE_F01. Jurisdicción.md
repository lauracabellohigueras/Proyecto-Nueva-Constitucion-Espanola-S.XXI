## TITULO Sexto - De la Justicia y los medios de fiscalización ##
###### Art. F01 - Jurisdicción ###### 
1. El ejercicio de la potestad jurisdiccional en todo tipo de procesos, juzgando y haciendo ejecutar lo juzgado, corresponde exclusivamente a los Juzgados y Tribunales determinados por las leyes, según las normas de competencia y procedimiento que las mismas establezcan.

> **Concordancias:**
>
> **Equivalencia CE78: 
>
