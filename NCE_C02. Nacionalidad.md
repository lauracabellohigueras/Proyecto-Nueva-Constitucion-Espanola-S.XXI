#### CAPITULO I. - De los españoles y los extranjeros

###### Art. C02 - Nacionalidad

1. Tienen derecho a la nacionalidad española "por nacimiento" los hijos de padre o madre españoles, los nacidos en territorio español cuya filiación no resulte determinada o a los que no se les pueda atribuir, en el momento de su nacimiento, otra nacionalidad; además de aquellos otros que determine la ley.
2. Tienen derecho a la nacionalidad española "por naturalización" aquellas personas que contraigan matrimonio o se unan mediante figura análoga legalmente reconocida en España con un español o española, y aquellos otros que determine la ley.
3. Ningún español "por nacimiento" podrá ser privado de su nacionalidad ni la perderá por adquirir otra, siendo ésta la única distinción posible entre españoles en función de la forma en que se accedió a la nacionalidad. El resto de españoles sólo podrán ser privados de la misma mediante sentencia judicial firme cuando así se haya previsto en las leyes.
4. Se puede renunciar a la nacionalidad española mediante mera comunicación al registro civil. Quien renuncie a ella podrá recuperarla previa solicitud, una vez verificadas las condiciones que dan derecho a la misma.
5. El Gobierno podrá concertar tratados de doble nacionalidad con aquellos países que hayan tenido o tengan una particular vinculación con España.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 11](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a11): Regulación de la nacionalidad, 
>



