## TITULO PRIMERO - De España, el Estado y la Nación Española ##
###### Art. A01 - Soberanía de la Nación española ######

1. La nación española ostenta el pleno derecho para gobernarse sin interferencias ni injerencias ajenas a si misma, y es la autoridad suprema en el territorio que habita. Esta soberanía es inalienable e imprescriptible; y se ejerce de forma directa o mediante delegación en los poderes del Estado.
2. Cuando no fuere posible otro recurso, los españoles tienen el deber de resistir por todos los medios posibles contra cualquier acto que atente, vulnere o menoscabe la Constitución.

> **Concordancias:**
>
> **Equivalencia CE78: [Art.1](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a1) [Art.23](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a23)

[Anterior](NCE_000_Preambulo.md) | [Siguiente](NCE_A02.%20Organizaci%C3%B3n%20pol%C3%ADtica%20de%20la%20Naci%C3%B3nEstado.md)