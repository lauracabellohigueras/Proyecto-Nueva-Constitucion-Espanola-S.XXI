#### CAPITULO SÉPTIMO - De las formas de participación ciudadana ####
###### Art. G.01 - Procedimiento electoral

1. El proceso para la elección y cese de las personas que ocupan puestos de carácter electivo o de confianza en la Administración pública y los órganos constitucionales, para la ratificación de decisiones políticas o normas con fuerza de ley, así como para la toma de decisiones colectivas vinculantes deben garantizar que:
 1. El sufragio se ejerce libremente, sin ningún tipo de coacción,  y que su contenido es secreto.
 2. Cada voto tiene el mismo valor.
 3. TODO: Voto directo.
 4. Una persona corriente sin conocimientos o medios extraordinarios puede comprender la forma en que los votos son emitidos y contabilizados, y pueda verificar todas las fases del procedimiento electoral por iniciativa propia.

> **Concordancias:**
>
> **Equivalencia CE78:** 
>
>

* **2020-02-25** *@rmatelec*:
