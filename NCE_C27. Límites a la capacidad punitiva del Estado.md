﻿###### Art. C.27 - Límites a la capacidad punitiva del Estado

1. Nadie puede ser condenado más de una vez por un mismo hecho, ni por aquellos en los que la capacidad de elección estuviera anulada ni legitima defensa ni extrema nacasidad.
2. Nadie puede ser condenado o sancionado por acciones u omisiones que en el momento de producirse no se encuentren definidas de forma expresa, precisa e inequívoca como punibles en el Código Penal o el Código de Derecho Administrativo Sancionador vigente en aquel momento.
3. Sólo podrán imponerse las sanciones y/o penas expresamente establecidas en la ley que define las acciones u omisiones como punibles previa demostración, mediante el procedimiento legalmente establecido, de que la actuación del condenado se corresponde exactamente con una conducta punible.
4. La sanción o pena a aplicar será la prevista en la legislación vigente en el momento de la realización de la conducta punible. La sanción se reducirá proporcionalmente si, durante su ejecución, se establecieran penas menores para la conducta punible. Si dejara de ser punible, tanto el procedimiento sancionador como la ejecución de la condena finalizarán inmediatamente.
5. Las sanciones y/o penas deberán ser proporcionadas a la lesión producida en el bien jurídicamente protegido y estar orientadas a la reparación del daño, la pacificación de la convivencia y la reinserción social.
6. Las sanciones económicas fuera del ámbito penal deberán ser proporcionales a la capacidad económica del sancionado, y nunca más lesivas que las que pudieran imponerse en el ámbito penal.
7. La pena de privación de libertad sólo podrá ser impuesta en el ámbito penal, y nunca como consecuencia directa o derivada de penas o sanciones no penales. Durante el cumplimiento de la misma el condenado gozará de todos sus derechos fundamentales, a excepción de los que se vean expresamente limitados por el contenido del fallo condenatorio, el sentido de la pena y la ley penitenciaria.
8. Queda prohibidas las penas de muerte, de trabajos forzados o de duración indefinida, salvo lo que puedan disponer el Código Penal para tiempos de guerra.

> **Concordancias:** [C01 - Ámbito y regulación de los derechos fundamentales](NCE_C01.%20Ámbito%20y%20regulación%20de%20los%20derechos%20fundamentales.md), B.02 
>
> **Equivalencia CE78:** [Art. 25](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a25),  [Art. 15](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a15) Derecho a la vida
>
> Principios contemplados: Reserva de ley, prohibición de retroactividad desfavorable, lex stricta / analogía, lex certa

Quien acusa debe probar
