### PREÁMBULO - Nación, Soberanía y Estado

Los hombres y mujeres de España,

1) en nuestro deseo de promover la realización de los más altos ideales humanos y de contribuir a la erradicación definitiva de la miseria, la violencia y de toda forma de opresión;

2) reconociéndonos mutuamente como integrantes de una comunidad politico-cultural nacional configurada a lo largo de la historia, con características e intereses propios y compartidos;

3) provistos de la capacidad de decidir, sin injerencias ajenas a nosotros mismos, cómo gobernarnos y el tipo de sociedad en la que queremos vivir;

4) conscientes del peligro de que el Estado utilice sus facultades en provecho propio, o contra quienes debe servir y proteger;

5) y sabedores de que sólo un poder está en disposición de frenar los abusos de otro;

hemos reunido nuestras voluntades para dotarnos de un marco institucional capaz de salvaguardar la libertad política colectiva fundado sobre la completa separación entre Nación y Estado, el acatamiento de las leyes que la Nación se ha dado a sí misma, el respeto a no hacer aquello que las normas no obligan, y sobre la prudente desconfianza hacia el Gobierno y cualquier otra forma de poder.

Diseñamos un Estado fuerte, capaz de cumplir los fines que justifican su existencia, pero plenamente sometido a la voluntad colectiva de la ciudadanía mediante un sistema político democrático, basado en la participación libre y en igualdad de condiciones, y sujeto al escrutinio constante de sus acciones y decisiones. Un Estado que actúe en función de las preferencias de la mayoría, pero respetando siempre la libertad y derechos de los individuos y las minorías; dividido en segmentos independientes y enfrentados, separados en origen y situados en un equilibrio de fuerzas estable para que ninguno de ellos pueda dominar a los demás, ni ser dominados por ellos.

Desgraciadamente, cuando toda persona es susceptible de ser corrompida, cuando toda organización puede acabar siendo subvertida para su instrumentalización con fines espurios, los controles institucionales no siempre son suficientes. La complacencia de las generaciones que nos precedieron propició un retroceso sin precedentes en nuestra libertad y bienestar a manos de un Estado incontrolado.

Esta debacle nos hizo redescubrir importantes verdades que nunca debieron de ser olvidadas: que los derechos sólo existen mientras se ejercen y defienden activamente, que en toda decisión política hay beneficiados y perjudicados, que es más fácil sacrificar los intereses de quienes no participan en la toma de decisiones y más difícil recuperar lo perdido que conservar lo que se tiene, que quienes más tienen que ganar són los que primero tratarán de convencernos para que apoyemos sus intereses en perjuicio de los nuestros, que los enemigos de nuestra libertad no descansan nunca, y que tanto la indiferencia como la impunidad alientan la injusticia y el crimen. Por fortuna aprendimos y reaccionamos.

No nos pareció sensato volver a confiar en la bondad, rectitud y honradez de funcionarios, políticos y gobernantes. Por eso, organizamos el Poder al servicio de nuestra libertad mediante la creación de instituciones capaces de salvaguardarla de los peores defectos de la naturaleza humana, y plasmamos dichas instituciones en esta Constitución. Nos dotamos de los mecanismos necesarios para mantener al Estado sometido a nuestra voluntad colectiva y al escrutinio público, de modo que sea la Nación la que lo gobierne, y no al revés. 

Estos derechos e instituciones no pueden darse por garantizados. La fuerza de toda Constitución procede de las personas dispuestas a hacerla cumplir, que no siempre se encuentran en las instituciones del Estado. El único escudo contra la tiranía es nuestra determinación de mantenernos libres, de hacer que el Estado respete los límites que le hemos impuesto, y nuestra disposición a realizar los sacrificios que sean necesarios para preservar el modo de vida que hemos elegido. Sabemos bien que el precio de la libertad es la eterna vigilancia.

Sólo una sociedad civil activa, informada, vigilante, crítica y responsable; guiada por la razón, el respeto, la decencia y la búsqueda de la verdad sin importar las consecuencias; libre de manipulaciones, odio y militancia ciega; en la que pensar de forma diferente no sea un agravio para nadie y se pueda discrepar civilizadamente, reconociendo el mérito de los demás y aceptando la crítica hacia lo propio; compuesta de personas autónomas y autosuficientes, que sepan que el Estado vive de ellos y existe para servirles, y que promuevan activamente sus intereses en el proceso político, está en disposición de gobernar a su Estado.

Por eso, os animamos a que hagáis vuestros los ideales que inspiran esta Constitución y asumáis como propia la misión de protegerla, perfeccionarla, y transmitir este legado de libertad y democracia a las generaciones venideras. Que vuestro ejemplo ilumine el camino hacia la prosperidad y la emancipación de todos los pueblos de la Tierra.

> **Concordancias:** Arts. A.02 - Articulación política del Estado y división de poderes, A03 - Legitimación y fines del estado
>
> **Equivalencia CE78:** Preámbulo

* **2016-04-21** *@rmatelec*: Normalmente los preámbulos de las Constituciones contienen declaraciones grandilocuentes de muy buenas intenciones que luego no se materializan en nada. En este caso, manteniendo la retórica, se trata de exponer las ideas elementales que subyacen a las luchas de poder, un breve contexto histórico en el que se promulgará esta Constitución, y ofrecer unas mínimas pautas de lo que se espera de todo ciudadano... Es decir, una especie de manual de instrucciones de la Constitución.

[Siguiente](NCE_A01.%20Soberan%C3%ADa%20de%20la%20Naci%C3%B3n%20espa%C3%B1ola.md)
