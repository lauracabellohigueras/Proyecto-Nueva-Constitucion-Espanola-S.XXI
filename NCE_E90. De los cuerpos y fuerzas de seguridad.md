
###### Art. E.90. De los cuerpos y fuerzas de seguridad del estado 

1. Los cuerpos y fuerzas de seguridad del estado tienen como misión garantizar el cumplimiento de la Constitución y las leyes protegiendo simultáneamente los derechos fundamentales de todas las personas y su libre ejercicio.

2.  En el ejercicio de sus funciones, sus miembros deberán portar signos distintivos claramente visibles en todo momento que permitan determinar el cuerpo o fuerza a la que pertenecen y permitan la fácil e inequívoca identificación de cada persona individualmente.

3. El uso de la fuerza sólo estará permitido cuando sea estrictamente necesario y en la medida en que lo requiera el desempeño de sus funciones.

4. Estos cuerpo y sus miembros actuaran en todo momento con justicia, integridad , diligencia, imparcialidad y respeto hacia todas las personas y había su privacidad.

5. Los miembros de estos cuerpos están obligados a ignorar cualquier orden recibida de persona sin competencia para emitirla o cuando serán contrarias al ordenamiento jurídico.

6. Mediante Ley Orgánica se determinará las funciones, principios básicos de actuación y estatutos de las Fuerzas y Cuerpos de seguridad.

> **Concordancias:** 
>
> **Equivalencia CE78:** [Art. 104](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a104): Cuerpos y fuerzas de seguridad del estado.
