#### CAPITULO II - De los Jueces y Magristrados ####

1. La ley orgánica del poder judicial determinará la constitución, funcionamiento y gobierno de los Juzgados y Tribunales, así como el estatuto jurídico de los Jueces y Magistrados de carrera, que formarán un Cuerpo único, y del personal al servicio de la Administración de Justicia.


Los conflictos de competencia que puedan producirse entre Juzgados o Tribunales de distinto orden jurisdiccional, integrados en el Poder Judicial, se resolverán por una Sala especial del Tribunal Supremo

> **Concordancias:**
>
> **Equivalencia CE78: [Art.122](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a122)
>
