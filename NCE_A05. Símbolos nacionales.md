###### Art. A05 - Símbolos nacionales ######
1. La bandera de España está formada por tres franjas horizontales, roja, amarilla y roja; siendo la amarilla de doble anchura que cada una de las rojas.
2. Las _Asambleas de Representantes_ de las entidades territoriales administrativas inferiores al Estado podrán establecer banderas y enseñas propias, que utilizarán junto a la bandera de España en sus edificios públicos y en sus actos oficiales.
3. El resto de símbolos nacionales, así como su uso, será regulado mediante ley.

> **Concordancias:**
>
>
> **Equivalencia CE78:**
>

[Anterior](NCE_A04.%20Territorio%20nacional.md) | [Siguiente](NCE_A06.%20Lenguas%20oficiales.md)