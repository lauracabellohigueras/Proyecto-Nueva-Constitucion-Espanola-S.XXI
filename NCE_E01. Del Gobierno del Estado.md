## TITULO Quinto - Del Poder Ejecutivo ##
###### Art. E01 - Del Gobierno del Estado ######

1. El Gobierno del Estado es el máximo órgano del poder ejecutivo y tiene encomendada la dirección de la Administración pública; de las fuerzas armadas y de orden público; el ejercicio de la potestad reglamentaria;  la representación del Estado fuera del territorio nacional; y las demás funciones comunes a todos los órganos del Estado.

2. El Gobierno se compone del Presidente, de los Vicepresidentes, en su caso, de los Ministros y de los demás miembros que establezca la ley.

3. El Presidente del Gobierno será elegido en circunscripción única nacional mediante sufragio universal directo por la mayoría absoluta de votos válidos emitidos. El procedimiento electoral deberá contar con los mecanismos necesarios para que dicho resultado pueda alcanzarse en segunda o sucesivas rondas, de no alcanzarse en la primera.

4. El Presidente del Gobierno ejercerá sus funciones por un periodo de cuatro años desde la proclamación oficial del resultado, cesando inmediatamente en el cargo en caso de dimisión, muerte, condena judicial firme que conlleve la inhabilitación para el ejercicio del cargo, moción de censura de la Asamblea Nacional o declaración de incapacidad aprobada por más de 3/5 de resto de miembros del Gobierno.

5. El caso de ausencia del Presidente del Gobierno o de quedar el cargo vacante, la presidencia del Gobierno será ejercida por el vicepresidente de mayor rango, por el siguiente miembro del Gobierno en el orden de sucesión establecido por el Presidente, o por la siguiente persona en el orden de sucesión establecido legalmente. En caso de ninguna persona fuera elegible para ocupar el cargo, se procederá a la convocatoria de nuevas elecciones presidenciales. 

6. El resto de miembros del Gobierno serán nombrados por decisión del Presidente del Gobierno, y cesan en sus funciones por decisión de éste.

> **Concordancias:** [Anterior](NCE_A02.%20Organizaci%C3%B3n%20pol%C3%ADtica%20de%20la%20Naci%C3%B3nEstado.md)
>
> **Equivalencia CE78: [Art.97](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a97) y siguientes
>
