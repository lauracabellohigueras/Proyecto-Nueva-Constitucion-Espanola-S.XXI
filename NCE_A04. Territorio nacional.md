###### Art. A04 - Territorio nacional

1. España abarca el territorio históricamente delimitado en los continentes europeo y africano, los archipiélagos de Baleares y Canarias, las ciudades de Ceuta y Melilla y el resto de plazas sobre las que ejerce soberanía; así como las islas adyacentes, el mar territorial, la plataforma submarina, el subsuelo y el espacio suprayacente a estos.
2. El Estado no puede enajenar parte alguna del territorio español ni los derechos de soberanía que ejerza sobre aquel, sin perjuicio de las rectificaciones de fronteras.
3. La capital de España, sede de las máximas instituciones del Estado, es el municipio de Madrid. Mediante norma con rango de ley podrá cambiarse su emplazamiento o ubicarse alguna de dichas instituciones en otra localidad.

> **Concordancias:**
>
> **Equivalencia CE78:** [Art. 5](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a5): Territorio.
>

[Anterior](NCE_A03.%20Articulaci%C3%B3n%20pol%C3%ADtica%20del%20Estado%20y%20divisi%C3%B3n%20de%20poderes.md) | [Siguiente](NCE_A05.%20S%C3%ADmbolos%20nacionales.md)