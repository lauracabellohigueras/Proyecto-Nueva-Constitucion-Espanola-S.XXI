#### CAPITULO  01 - Derechos Fundamentales

###### Art. C.20 - Derecho a la vida y a la integridad humana

1. Toda persona tiene derecho a la vida, a desarrollar libremente su personalidad de acuerdo a su propia voluntad, a la integridad física y mental, y a ser tratada con dignidad y respeto.
2. Nadie podrá ser sometido a torturas, trabajos forzados, ni a situaciones o tratos crueles, inhumanos o degradantes.
3. Nadie podrá ser obligado a vivir en contra de su voluntad.

> **Concordancias:** C.04 - Sobre la definición de persona. C01. 
>
> **Equivalencia CE78:** [Art. 15](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a15): Derecho a la vida e integridad física.  [Art. 10](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a10): Fundamentos del orden político y de la paz social

 | [Siguiente](NCE_21.%20Derecho%20a%20la%20libertad%20de%20pensamiento.md)
 